import numpy as np
from sklearn import metrics
import matplotlib.pyplot as plt
from pathlib import Path

script_directory = Path(__file__).resolve().parent
y = np.array([1, 1, -1, -1, 1, 1, -1, -1, 1, -1])
scores = np.array([0.61, 0.03, 0.68, 0.31, 0.45, 0.09, 0.38, 0.05, 0.01, 0.04])
print(np.sort(scores))
fpr, tpr, thresholds = metrics.roc_curve(y, scores)
print('TPR', tpr)
print('FPR', fpr)

plt.figure()
plt.plot(fpr, tpr, color='darkorange')
plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curve')

plt.savefig(f'{script_directory}/../output/Assignment_3_1_roc_plot_from_function.png')
plt.clf()

# manual calculation results

fpr_manual = np.array([1, 1, 1, 0.8, 0.6, 0.6, 0.4, 0.2, 0.2, 0.2, 0])
tpr_manual = np.array([1, 0.8, 0.6, 0.6, 0.6, 0.4, 0.4, 0.4, 0.2, 0, 0])

plt.figure()
plt.plot(fpr_manual, tpr_manual, color='darkorange')
plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curve')

plt.savefig(f'{script_directory}/../output/Assignment_3_1_roc_plot_manual.png')
