from sklearn import svm, neighbors, ensemble, neural_network
import Assignment_4_data_inits as Data_loader

training_matrix_data, training_labels_data = Data_loader.load_digits_training_data()
testing_matrix_data, testing_labels_data = Data_loader.load_digits_test_data()

poly_svm_model = svm.SVC(kernel='poly', degree=2, gamma='auto')
k_nearest_model = neighbors.KNeighborsClassifier(n_neighbors=7)
fnn_model = neural_network.MLPClassifier(hidden_layer_sizes=25, max_iter=1000)

poly_svm_model.fit(training_matrix_data, training_labels_data.ravel())
poly_svm_score = poly_svm_model.score(testing_matrix_data, testing_labels_data.ravel())
print('SVM score', poly_svm_score)

k_nearest_model.fit(training_matrix_data, training_labels_data.ravel())
k_nearest_score = k_nearest_model.score(testing_matrix_data, testing_labels_data.ravel())
print('K nearest score', k_nearest_score)

fnn_model.fit(training_matrix_data, training_labels_data.ravel())
fnn_model_score = fnn_model.score(testing_matrix_data, testing_labels_data)
print('FNN score', fnn_model_score)

voting_model = ensemble.VotingClassifier(estimators=[('k_nearest', k_nearest_model),
                                                     ('svm_poly', poly_svm_model),
                                                     ('fnn', fnn_model)], voting='hard')
voting_model = voting_model.fit(training_matrix_data, training_labels_data.ravel())
# predict the labels on validation dataset
voting_accuracy = voting_model.score(testing_matrix_data, testing_labels_data)
print("Accuracy with Ensemble(majority voting) ", voting_accuracy)
