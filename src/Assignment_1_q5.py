import matplotlib.pyplot as plt
import numpy as np


temps = np.array([14.2, 16.4, 11.9, 15.2, 18.5, 22.1, 19.4, 25.1, 23.4, 18.1, 22.6, 17.2])
ice_cream_sales = np.array([215, 325, 185, 332, 406, 522, 412, 614, 544, 421, 445, 408])

correlation = np.corrcoef(temps, ice_cream_sales)

std_temp = np.std(temps)
std_sales = np.std(ice_cream_sales)
print('mean_temp', np.mean(temps), 'mean_sales', np.mean(ice_cream_sales))
print('std_temp', std_temp, 'std_sales', std_sales)
covar_temp_sales = np.cov(temps, ice_cream_sales)
print('cov_temp_sales', covar_temp_sales)

print('correlation', correlation)

plt.scatter(temps, ice_cream_sales)
plt.xlabel('Temperature(Celsius)')
plt.ylabel('Ice Cream Sales')
plt.show()
