import scipy.io
from pathlib import Path
import numpy as np

script_directory = Path(__file__).resolve().parent
digits_data_path = Path(f'{script_directory}/../resources/Assignment_4_data/Handwritten Digits')
multi_label_scene_data_path = Path(f'{script_directory}/../resources/Assignment_4_data/Multi Label Scene Data')
seeds_data_path = Path(f'{script_directory}/../resources/Assignment_4_data/Seeds Data')


def load_training_data(dataset_type):
    if dataset_type == 'digits':
        data_path = digits_data_path
    else:
        data_path = multi_label_scene_data_path

    training_matrix_file_path = data_path / f'X_train.mat'
    training_labels_file_path = data_path / f'y_train.mat'

    training_matrix_mat = scipy.io.loadmat(str(training_matrix_file_path))
    training_label_mat = scipy.io.loadmat(str(training_labels_file_path))

    return training_matrix_mat['X_train'], training_label_mat['y_train']


def load_test_data(dataset_type):
    if dataset_type == 'digits':
        data_path = digits_data_path
    else:
        data_path = multi_label_scene_data_path

    testing_matrix_file_path = data_path / f'X_test.mat'
    testing_labels_file_path = data_path / f'y_test.mat'

    testing_matrix_mat = scipy.io.loadmat(str(testing_matrix_file_path))
    testing_labels_mat = scipy.io.loadmat(str(testing_labels_file_path))

    return testing_matrix_mat['X_test'], testing_labels_mat['y_test']


def load_digits_training_data():
    return load_training_data('digits')


def load_digits_test_data():
    return load_test_data('digits')


def load_scene_training_data():
    return load_training_data('scene')


def load_scene_testing_data():
    return load_test_data('scene')


def load_seeds_data():
    seeds_file_path = seeds_data_path / 'seeds.txt'
    return np.loadtxt(seeds_file_path)
