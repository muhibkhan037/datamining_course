import scipy.spatial.distance as sci

a = [1, 2, 3, 4, 5]
b = [5, 4, 3, 2, 1]

eu_distance = sci.euclidean(a, b)
co_sim = sci.cosine(a, b)

print('Euclidean ', eu_distance)
print('Cosign similarity ', 1-co_sim)
