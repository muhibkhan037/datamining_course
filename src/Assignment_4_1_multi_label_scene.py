import Assignment_4_data_inits as Data_loader
from sklearn.svm import SVC
import numpy as np

training_matrix_data, training_labels_data = Data_loader.load_scene_training_data()
testing_matrix_data, testing_labels_data = Data_loader.load_scene_testing_data()


C_2d_range = [0.1, 1.0, 10.0, 100.0]
for C_val in C_2d_range:
    print('For C', C_val)
    svm_poly_classifier_models = []
    svm_gauss_classifier_models = []
    for class_no in range(0, training_labels_data.shape[1]):
        # print('Training SVM poly for', class_no)
        training_labels_data_for_class = training_labels_data[:, class_no]
        svm_model = SVC(C=C_val, kernel='poly', degree=2, gamma='auto')
        svm_model.fit(training_matrix_data, training_labels_data_for_class)
        svm_poly_classifier_models.append(svm_model)

    for class_no in range(0, training_labels_data.shape[1]):
        # print('Training SVM gauss for', class_no)
        training_labels_data_for_class = training_labels_data[:, class_no]
        svm_model = SVC(C=C_val, kernel='rbf', gamma='auto')
        svm_model.fit(training_matrix_data, training_labels_data_for_class)
        svm_gauss_classifier_models.append(svm_model)


    def get_accuracy_for_svm(svm_models):
        predicted_labels = np.array([])
        for class_number in range(0, testing_labels_data.shape[1]):
            # print('For class', class_number)
            classifier = svm_models[class_number]
            predictions = np.array(classifier.predict(testing_matrix_data))
            if len(predicted_labels) == 0:
                predicted_labels = predictions.reshape(predictions.shape[0], 1)
            else:
                predicted_labels = np.concatenate((predicted_labels, predictions.reshape(predictions.shape[0], 1)),
                                                  axis=1)

        accuracy_array = []
        for sample_no in range(0, testing_labels_data.shape[0]):
            true_labels_for_sample = testing_labels_data[sample_no]
            predicted_labels_for_sample = predicted_labels[sample_no]
            logical_and = np.logical_and(true_labels_for_sample, predicted_labels_for_sample)
            logical_or = np.logical_or(true_labels_for_sample, predicted_labels_for_sample)
            t_and_p_count = 0.0
            t_or_p_count = 0.0
            for i in range(0, len(logical_and)):
                if logical_and[i]:
                    t_and_p_count += 1
                if logical_or[i]:
                    t_or_p_count += 1
            accuracy = t_and_p_count / t_or_p_count
            accuracy_array.append(accuracy)

        accuracy_array = np.array(accuracy_array)
        avg_accuracy = np.average(accuracy_array)
        return avg_accuracy

    print('For SVM poly ', get_accuracy_for_svm(svm_poly_classifier_models))
    print('For SVM Gauss', get_accuracy_for_svm(svm_gauss_classifier_models))

