import numpy as np
from sklearn.linear_model import LogisticRegression
import Assignment_3_2_data_inits as dataInit
import matplotlib.pyplot as plt
from pathlib import Path

sample_size = 10
N = 5
log_reg_max_iter = 5000
script_directory = Path(__file__).resolve().parent


def get_rand_sample_indexes(unlabeled_data, sample_count):
    # get random index
    rand_sample_index = np.random.choice(unlabeled_data.shape[0], sample_count, replace=False)
    return rand_sample_index


def get_uncertainty_sample_indexes(log_reg, unlabeled_data, sample_count):
    # calculate probability and log probability and uncertainty
    unlabeled_probabilities = log_reg.predict_proba(unlabeled_data)
    unlabeled_probabilities_log = log_reg.predict_log_proba(unlabeled_data)

    probabilities_mul_log_probabilities = unlabeled_probabilities * unlabeled_probabilities_log
    uncertainty_sums = np.sum(probabilities_mul_log_probabilities, axis=1)

    # keep the sums in reverse sign to sum in descending order and pick the top values
    uncertainly_max_values_index = np.argsort(uncertainty_sums)[0:sample_count]
    return uncertainly_max_values_index


def active_learning_with_sampling(log_reg, sampling_type: str, training_matrix_data, training_labels_data,
                                  testing_matrix_data, testing_labels_data,
                                  unlabeled_matrix_data, unlabeled_labels_data):
    accuracy_values = np.zeros(N)
    for n in range(0, N):
        log_reg.fit(training_matrix_data, training_labels_data.ravel())
        # Use score method to get accuracy of model
        score = log_reg.score(testing_matrix_data, testing_labels_data.ravel())
        accuracy_values[n] = score

        if sampling_type == 'random':
            active_learning_sample_indices = get_rand_sample_indexes(unlabeled_matrix_data, sample_size)
        else:
            active_learning_sample_indices = get_uncertainty_sample_indexes(log_reg, unlabeled_matrix_data, sample_size)

        # add to training matrix and delete from unlabeled
        training_matrix_data = np.append(training_matrix_data, unlabeled_matrix_data[active_learning_sample_indices], axis=0)
        unlabeled_matrix_data = np.delete(unlabeled_matrix_data, active_learning_sample_indices, axis=0)

        # add to training labels and delete from unlabeled
        training_labels_data = np.append(training_labels_data, unlabeled_labels_data[active_learning_sample_indices], axis=0)
        unlabeled_labels_data = np.delete(unlabeled_labels_data, active_learning_sample_indices, axis=0)

    return accuracy_values


def compute_rand_sampling_accuracy_mind_reading_sets():
    rand_sample_acc_val_arrays = np.zeros((3, N))
    for set_no in range(1, 4):
        training_matrix, training_labels = dataInit.load_mind_reading_training_data(set_no=1)
        testing_matrix, testing_labels = dataInit.load_mind_reading_testing_data(set_no=1)
        unlabeled_matrix, unlabeled_labels = dataInit.load_mind_reading_unlabeled_data(set_no=1)

        logistic_reg = LogisticRegression(solver='lbfgs', max_iter=log_reg_max_iter, multi_class='auto')
        rand_sample_acc_val_arrays[set_no - 1] = active_learning_with_sampling(logistic_reg, 'random',
                                                                               training_matrix, training_labels,
                                                                               testing_matrix, testing_labels,
                                                                               unlabeled_matrix, unlabeled_labels)

    accuracy_values_mean = np.mean(rand_sample_acc_val_arrays, axis=0)
    return accuracy_values_mean


def compute_uncertainty_sampling_accuracy_mind_reading_sets():
    rand_sample_acc_val_arrays = np.zeros((3, N))
    for set_no in range(1, 4):
        training_matrix, training_labels = dataInit.load_mind_reading_training_data(set_no=1)
        testing_matrix, testing_labels = dataInit.load_mind_reading_testing_data(set_no=1)
        unlabeled_matrix, unlabeled_labels = dataInit.load_mind_reading_unlabeled_data(set_no=1)

        logistic_reg = LogisticRegression(solver='lbfgs', max_iter=log_reg_max_iter, multi_class='auto')
        rand_sample_acc_val_arrays[set_no - 1] = active_learning_with_sampling(logistic_reg, 'uncertainty',
                                                                               training_matrix, training_labels,
                                                                               testing_matrix, testing_labels,
                                                                               unlabeled_matrix, unlabeled_labels)

    accuracy_values_mean = np.mean(rand_sample_acc_val_arrays, axis=0)
    return accuracy_values_mean


def compute_rand_sampling_accuracy_mmi_sets():
    rand_sample_acc_val_arrays = np.zeros((3, N))
    for set_no in range(1, 4):
        training_matrix, training_labels = dataInit.load_mmi_training_data(set_no=1)
        testing_matrix, testing_labels = dataInit.load_mmi_testing_data(set_no=1)
        unlabeled_matrix, unlabeled_labels = dataInit.load_mmi_unlabeled_data(set_no=1)

        logistic_reg = LogisticRegression(solver='lbfgs', max_iter=log_reg_max_iter, multi_class='auto')
        rand_sample_acc_val_arrays[set_no - 1] = active_learning_with_sampling(logistic_reg, 'random',
                                                                               training_matrix, training_labels,
                                                                               testing_matrix, testing_labels,
                                                                               unlabeled_matrix, unlabeled_labels)

    accuracy_values_mean = np.mean(rand_sample_acc_val_arrays, axis=0)
    return accuracy_values_mean


def compute_uncertainty_sampling_accuracy_mmi_sets():
    rand_sample_acc_val_arrays = np.zeros((3, N))
    for set_no in range(1, 4):
        training_matrix, training_labels = dataInit.load_mmi_training_data(set_no=1)
        testing_matrix, testing_labels = dataInit.load_mmi_testing_data(set_no=1)
        unlabeled_matrix, unlabeled_labels = dataInit.load_mmi_unlabeled_data(set_no=1)

        logistic_reg = LogisticRegression(solver='lbfgs', max_iter=log_reg_max_iter, multi_class='auto')
        rand_sample_acc_val_arrays[set_no - 1] = active_learning_with_sampling(logistic_reg, 'uncertainty',
                                                                               training_matrix, training_labels,
                                                                               testing_matrix, testing_labels,
                                                                               unlabeled_matrix, unlabeled_labels)

    accuracy_values_mean = np.mean(rand_sample_acc_val_arrays, axis=0)
    return accuracy_values_mean


# calculate accuracies for Mind reading
print('Random Sampling  Mind Reading ...')
rand_sample_accuracy_values_mind_reading = compute_rand_sampling_accuracy_mind_reading_sets()
print('Random Sampling Mind Reading', 'Min accuracy', rand_sample_accuracy_values_mind_reading.min(),
      'Max accuracy', rand_sample_accuracy_values_mind_reading.max())

print('Uncertainty-based Sampling  Mind Reading ...')
uncertainty_sample_accuracy_values_mind_reading = compute_uncertainty_sampling_accuracy_mind_reading_sets()
print('Uncertainty-based Mind Reading', 'Min accuracy', uncertainty_sample_accuracy_values_mind_reading.min(),
      'Max accuracy', uncertainty_sample_accuracy_values_mind_reading.max())

# calculate accuracies for MMI
print('Random Sampling  MMI ...')
rand_sample_accuracy_values_mmi = compute_rand_sampling_accuracy_mmi_sets()
print('Random Sampling MMI', 'Min accuracy', rand_sample_accuracy_values_mmi.min(),
      'Max accuracy', rand_sample_accuracy_values_mmi.max())

print('Uncertainty-based Sampling  MMI ...')
uncertainty_sample_accuracy_values_mmi = compute_uncertainty_sampling_accuracy_mmi_sets()
print('Uncertainty-based Sampling', 'Min accuracy', uncertainty_sample_accuracy_values_mmi.min(),
      'Max accuracy', uncertainty_sample_accuracy_values_mmi.max())

# Initialize the plot
fig = plt.figure(figsize=(8, 6))
mind_reading_subplot = fig.add_subplot(211)
mmi_subplot = fig.add_subplot(212)

iteration_range = np.arange(1, N + 1, dtype=int)

mind_reading_subplot.plot(iteration_range, rand_sample_accuracy_values_mind_reading,
                          color='navy', marker='.', ls='--', label='Random')
mind_reading_subplot.plot(iteration_range, uncertainty_sample_accuracy_values_mind_reading,
                          color='crimson', marker='*', ls='--', label='Uncertainty-based')
mind_reading_subplot.set(title="Mind Reading DataSet", xlabel="Iteration", ylabel="Accuracy")
mind_reading_subplot.legend()

mmi_subplot.plot(iteration_range, rand_sample_accuracy_values_mmi,
                 color='navy', marker='.', ls='--', label='Random')
mmi_subplot.plot(iteration_range, uncertainty_sample_accuracy_values_mmi,
                 color='crimson', marker='*', ls='--', label='Uncertainty-based')
mmi_subplot.set(title="MMI DataSet", xlabel="Iteration", ylabel="Accuracy")
mmi_subplot.legend()
# Show the plot
plt.tight_layout()
# plt.show()
plt.savefig(f'{script_directory}/../output/Assignment_3_2_plot.png')

