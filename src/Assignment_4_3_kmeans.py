import Assignment_4_data_inits as Data_loader
import numpy as np


seeds = Data_loader.load_seeds_data()


class KMeans:
    def __init__(self, k=3, max_iterations=100, tolerance=0.001, data_set=seeds):
            self.k = k
            self.tolerance = tolerance
            self.max_iterations = max_iterations
            self.data_set = data_set
            self.clusters = {}
            self.centroids = []
            self.sse = 0.0

    def get_random_centroids(self):
        random_indices = np.random.choice(self.data_set.shape[0], self.k)
        return self.data_set[random_indices]

    def get_sse_value(self):
        sse = 0.0
        for cluster_no in range(self.k):
            cluster = self.clusters[cluster_no]
            centroid = self.centroids[cluster_no]

            for sample in cluster:
                squared_error = np.linalg.norm(sample - centroid)**2
                sse += squared_error
        return sse

    def run_kmeans(self):
        init_centroids = self.get_random_centroids()
        # print(init_centroids)
        self.centroids = init_centroids
        for i in range(self.max_iterations):
            self.clusters = {}
            for j in range(self.k):
                self.clusters[j] = []

            for sample in self.data_set:
                distances = [np.linalg.norm(sample - centroid) for centroid in self.centroids]
                cluster_index = distances.index(min(distances))
                self.clusters[cluster_index].append(sample)

            for j in range(self.k):
                self.centroids[j] = np.mean(self.clusters[j], axis=0)

            new_sse = self.get_sse_value()
            # print('old sse', self.sse, 'new sse', new_sse)
            if abs(new_sse - self.sse) < self.tolerance:
                # print('K-Means converged', 'SSE value', new_sse)
                self.sse = new_sse
                break
            else:
                self.sse = new_sse


def get_avg_sse_for_kmeans(k, iteration_count):
    sse_values = []
    for i in range(iteration_count):
        kmeans = KMeans(k=k, data_set=seeds, max_iterations=100)
        kmeans.run_kmeans()
        sse_values.append(kmeans.sse)
    sse_values = np.array(sse_values)
    return np.mean(sse_values)


print(get_avg_sse_for_kmeans(k=3, iteration_count=10))
print(get_avg_sse_for_kmeans(k=5, iteration_count=10))
print(get_avg_sse_for_kmeans(k=7, iteration_count=10))
