import scipy.io
from pathlib import Path

script_directory = Path(__file__).resolve().parent
mindReadingDataPath = Path(f'{script_directory}/../resources/Assignment_3_data/MindReading')
mmi_data_path = Path(f'{script_directory}/../resources/Assignment_3_data/MMI')


def load_mind_reading_training_data(set_no: int):
    training_matrix_file_path = mindReadingDataPath / f'trainingMatrix_MindReading{set_no}.mat'
    training_labels_file_path = mindReadingDataPath / f'trainingLabels_MindReading_{set_no}.mat'

    matrix_data = scipy.io.loadmat(str(training_matrix_file_path))['trainingMatrix']
    labels_data = scipy.io.loadmat(str(training_labels_file_path))['trainingLabels']
    return matrix_data, labels_data


def load_mind_reading_testing_data(set_no: int):
    testing_matrix_file_path = mindReadingDataPath / f'testingMatrix_MindReading{set_no}.mat'
    testing_labels_file_path = mindReadingDataPath / f'testingLabels_MindReading{set_no}.mat'
    matrix_data = scipy.io.loadmat(str(testing_matrix_file_path))['testingMatrix']
    labels_data = scipy.io.loadmat(str(testing_labels_file_path))['testingLabels']
    return matrix_data, labels_data


def load_mind_reading_unlabeled_data(set_no: int):
    unlabeled_matrix_file_path = mindReadingDataPath / f'unlabeledMatrix_MindReading{set_no}.mat'
    unlabeled_labels_file_path = mindReadingDataPath / f'unlabeledLabels_MindReading_{set_no}.mat'
    matrix_data = scipy.io.loadmat(str(unlabeled_matrix_file_path))['unlabeledMatrix']
    labels_data = scipy.io.loadmat(str(unlabeled_labels_file_path))['unlabeledLabels']
    return matrix_data, labels_data


def load_mmi_training_data(set_no: int):
    training_matrix_file_path = mmi_data_path / f'trainingMatrix_{set_no}.mat'
    training_labels_file_path = mmi_data_path / f'trainingLabels_{set_no}.mat'

    matrix_data = scipy.io.loadmat(str(training_matrix_file_path))['trainingMatrix']
    labels_data = scipy.io.loadmat(str(training_labels_file_path))['trainingLabels']
    return matrix_data, labels_data


def load_mmi_testing_data(set_no: int):
    testing_matrix_file_path = mmi_data_path / f'testingMatrix_{set_no}.mat'
    testing_labels_file_path = mmi_data_path / f'testingLabels_{set_no}.mat'
    matrix_data = scipy.io.loadmat(str(testing_matrix_file_path))['testingMatrix']
    labels_data = scipy.io.loadmat(str(testing_labels_file_path))['testingLabels']
    return matrix_data, labels_data


def load_mmi_unlabeled_data(set_no: int):
    unlabeled_matrix_file_path = mmi_data_path / f'unlabeledMatrix_{set_no}.mat'
    unlabeled_labels_file_path = mmi_data_path / f'unlabeledLabels_{set_no}.mat'
    matrix_data = scipy.io.loadmat(str(unlabeled_matrix_file_path))['unlabeledMatrix']
    labels_data = scipy.io.loadmat(str(unlabeled_labels_file_path))['unlabeledLabels']
    return matrix_data, labels_data
